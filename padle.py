from cocos.layer.util_layers import ColorLayer
from cocos.euclid import Vector2
from cocos.cocosnode import CocosNode
from cocos.director import director

class Padle(ColorLayer):

    is_event_handler = True

    def __init__(self,x,y):
        super(Padle, self).__init__(255,255,255,255,width=64,height=16)
        self.position=(x,y)

    def on_mouse_motion(self,x,y,dx,dy):
        self.position=(x-(self.width/2),self.position[1])
