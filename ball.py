from cocos.layer.util_layers import ColorLayer
from cocos.euclid import Vector2
from cocos.cocosnode import CocosNode
from cocos.director import director

SPEED=350

class Ball(ColorLayer):
    def __init__(self,x,y,dx,dy):
        super(Ball,self).__init__(255,255,255,255,width=16,height=16)
        self.position=(x,y)
        self.dir=Vector2(dx,dy)
        self.dir.normalize()
        self.schedule_interval(self.update,0.01)

    def update(self,dt):
        new_x=self.position[0]+(self.dir.x)*dt*SPEED
        new_y=self.position[1]+(self.dir.y)*dt*SPEED
        win_w,win_h=director.get_window_size()
        if new_y+self.height>win_h:
            new_y=self.position[1]-2*(self.dir.y)*dt*SPEED
            self.dir.y=(-1)*self.dir.y
        elif new_x+self.width>win_w or new_x<0:
            new_x=self.position[0]-2*(self.dir.x)*dt*SPEED
            self.dir.x=(-1)*self.dir.x
        elif new_y+self.height<0:
            exit()
        self.position=(new_x,new_y)
