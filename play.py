from cocos.scene import Scene
from cocos.layer import Layer
from cocos.layer.util_layers import ColorLayer
from ball import Ball
from padle import Padle

def create_play_scene():
    background_layer=ColorLayer(0,0,0,255)
    main_layer=Layer()
    main_layer.add(Ball(24,16,1,3))
    main_layer.add(Padle(0,0))
    scene=Scene(background_layer,main_layer)
    return(scene)
