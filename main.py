from cocos.director import director
from play import create_play_scene

def main():
  director.init()
  director.run(create_play_scene())

if __name__ == '__main__':
    main()
